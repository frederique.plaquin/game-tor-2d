from View.Fenetre import Fenetre
from View.MenuConnexion import MenuConnexion
from View.MenuRegister import MenuRegister
from View.MenuAccueil import MenuAccueil
from View.Plateforme import Plateforme
from Controller.saveUser import SaveUser
from Controller.createCaracterPickle import CreatePersonnagePickle
from Model.model import Model


class Controller:
    def __init__(self, **kwargs):
        self.model = Model()
        self.fenetre = Fenetre()
        self.menu_connexion = MenuConnexion()
        self.menu_register = MenuRegister(self)
        self.menu_accueil = MenuAccueil(self)
        # self.model_size = self.model.size
        self.plateforme = Plateforme(self, self.model)

        self.fenetre.afficher_menu(self.menu_accueil)

    def afficher_menu(self, menu):
        self.fenetre.afficher_menu(menu)

    def clbk_menu_register(self, widget):
        self.afficher_menu(self.menu_register)

    def clbk_plateforme(self, widget):
        # UserAfficher(widget)
        # self.menus.menu_images.layout1()
        # self.menus.afficher_menu(self.menus.menu_images)
        self.afficher_menu(self.plateforme)

    def clbk_menu_accueil(self, widget):
        # self.menu_register.grid1.clear_widgets()
        self.afficher_menu(self.menu_accueil)
        pass

    def save_user(self, widget):
        user_nom = self.menu_register.input_name.text
        user_prenom = self.menu_register.input_prenom.text
        user_age = self.menu_register.input_age.text
        SaveUser(user_nom, user_prenom, user_age)
        CreatePersonnagePickle(self.model.caracter)
        self.menu_accueil.layout1()

    def on_button_click(self):
        print("in on_button_click")

    def on_keyboard_closed(self):
        print('in on_keyboard_closed')
        self.plateforme.keyboard.unbind(on_key_down=self.on_key_down)
        self.plateforme.keyboard = None

    def on_key_down(self, keyboard, keycode, text, modifiers):
        currentx = self.plateforme.personnage.pos_hint["center_x"]
        currenty = self.plateforme.personnage.pos_hint["center_y"]

        print(currentx)
        print(currenty)

        if text == "z":
            if currenty > 1:
                currenty = 0
            else:
                currenty += 0.1
        if text == "s":
            if currenty < 0:
                currenty = 1
            else:
                currenty -= 0.1
        if text == "q":
            if currentx < 0:
                currentx = 1
            else:
                currentx -= 0.1
        if text == "d":
            if currentx > 1:
                currentx = 0
            else:
                currentx += 0.1

        self.plateforme.personnage.pos_hint = {"center_x": currentx, "center_y": currenty}


