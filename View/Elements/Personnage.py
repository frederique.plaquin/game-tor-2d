from kivy.uix.image import Image, AsyncImage
from kivy.core.window import Window

class Personnage(Image):
    def __init__(self, model, **kwargs):
        Image.__init__(self, **kwargs)
        self.model = model
        self.caracter = model.personnage()
        self.source = self.model.caracter[2]
        self.size_hint = (0.2, 0.2)
        self.pos_hint = {"center_x": 0.5, "center_y": 0.5}
