from kivy.uix.button import Button

class DirectionButton(Button):
    def __init__(self, text, pos_hint, **kwargs):
        Button.__init__(self, **kwargs)
        self.text = text
        self.size_hint = (0.1, 0.1)
        self.pos_hint = pos_hint
