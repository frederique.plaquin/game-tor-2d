from kivy.uix.floatlayout import FloatLayout
from kivy.uix.button import Button
from View.Elements.Personnage import Personnage
from View.Elements.DirectionButton import DirectionButton
from kivy.graphics import Rectangle, Color
from kivy.core.window import Window
import Variables.VARIABLES as Vars


class Bouton(Button):
    def __init__(self, **kwargs):
        Button.__init__(self, **kwargs)
        # self.background_color = background_color


class Plateforme(FloatLayout):
    def __init__(self, menus, model, **kwargs):
        FloatLayout.__init__(self, **kwargs)
        self.menus = menus
        self.model = model
        self.grid1 = FloatLayout()
        self.grid1.canvas.add(Color(1, 1, 1))
        self.grid1.canvas.add(Rectangle(size=Window.size))
        self.add_widget(self.grid1)
        self.layout1()
        self.keyboard = Window.request_keyboard(self.menus.on_keyboard_closed, self)
        self.keyboard.bind(on_key_down=self.menus.on_key_down)

    def layout1(self):
        self.grid1.clear_widgets()
        self.personnage = Personnage(self.model)
        self.grid1.add_widget(self.personnage)
        self._make_buttons()
        bouton_quitter = Button(text="Sauvegarder et Quitter", size_hint=(0.2, 0.1), pos_hint={'x': 0, 'y': 0})
        bouton_quitter.bind(on_press=self.menus.clbk_menu_accueil)
        self.grid1.add_widget(bouton_quitter)

    def _make_buttons(self):
        directions = Vars.DIRECTIONS
        self.boutons_directions = []
        for item in directions:
            self.boutons_directions.append(item)
        for item in self.boutons_directions:
            if item == "Haut":
                self.pos_hint = {'x': .8, 'y': 0.2}
                self.button_up = DirectionButton(text=item, pos_hint=self.pos_hint)
                self.button_up.bind(on_press=self.menus.on_button_click)
                self.grid1.add_widget(self.button_up)
            elif item == "Bas":
                self.pos_hint = {'x': .8, 'y': 0}
                self.button_down = DirectionButton(text=item, pos_hint=self.pos_hint)
                self.button_down.bind(on_press=self.menus.on_button_click)
                self.grid1.add_widget(self.button_down)
            elif item == "Gauche":
                self.pos_hint = {'x': .7, 'y': 0.1}
                self.button_right = DirectionButton(text=item, pos_hint=self.pos_hint)
                self.button_right.bind(on_press=self.menus.on_button_click)
                self.grid1.add_widget(self.button_right)
            elif item == "Droite":
                self.pos_hint = {'x': .9, 'y': 0.1}
                self.button_left = DirectionButton(text=item, pos_hint=self.pos_hint)
                self.button_left.bind(on_press=self.menus.on_button_click)
                self.grid1.add_widget(self.button_left)
