from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput
from View.Texte.Bouton import Bouton
from Controller.saveUser import SaveUser


class MenuRegister(GridLayout):
    def __init__(self, menus, **kwargs):
        GridLayout.__init__(self, cols=1, rows=1, **kwargs)
        self.menus = menus
        self.grid1 = GridLayout(cols=2, rows=4)
        self.add_widget(self.grid1)
        self.layout1()

    def layout1(self):
        self.label_name = Label(text="Nom")
        self.input_name = TextInput(text="")
        self.label_prenom = Label(text="Prénom")
        self.input_prenom = TextInput(text="")
        self.label_age = Label(text="Age")
        self.input_age = TextInput(text="")
        self.register_button = Bouton(text="S' inscrire")
        self.cancel_button = Button(text="Annuler")
        self.register_button.bind(on_press=self.menus.save_user)
        self.cancel_button.bind(on_press=self.menus.clbk_menu_accueil)
        self.grid1.add_widget(self.label_name)
        self.grid1.add_widget(self.input_name)
        self.grid1.add_widget(self.label_prenom)
        self.grid1.add_widget(self.input_prenom)
        self.grid1.add_widget(self.label_age)
        self.grid1.add_widget(self.input_age)
        self.grid1.add_widget(self.register_button)
        self.grid1.add_widget(self.cancel_button)

