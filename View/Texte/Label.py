from kivy.uix.label import Label


class Texte(Label):
    def __init__(self, **kwargs):
        Label.__init__(self, **kwargs)
        self.bind(size=self.set_size)

    def set_size(self, widget, value):
        widget.text_size = self.size
