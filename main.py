import kivy
kivy.require('2.0.0')  # replace with your current kivy version !

from kivy.app import App

from Controller.controller import Controller


class GameApp(App):

    def build(self):
        menus = Controller()
        return menus.fenetre


if __name__ == '__main__':
    GameApp().run()
